<?php
session_start();

include "conn.php";
include "functions/functions.php";

$title = setTitle('Movie detail | ' . getSetting()['app_name']);
$_SESSION['nav_active'] = 'movie';

if(!empty($_GET['id'])){
    $id = $_GET['id'];
    $film = getData('film', 5 , ['field' => 'rating', 'type' => 'DESC']);
    $single = get_where('film',['idfilm' => $id]);
    $q = "SELECT * FROM user_review JOIN users USING(iduser) WHERE idfilm='$id'";
    $comment = query($q,true,['comment_at' => 'DESC']);
    $hour = floor(strval($single['durasi']) / 60); 
    $min = strval($single['durasi']) % 60;


    $arr = [];
    $arr[] = ['title' => 'Director', 'value' => $single['sutradara']];
    $arr[] = ['title' => 'Country', 'value' => $single['negara']];
    $arr[] = ['title' => 'Year', 'value' => $single['tahunrilis']];
    $arr[] = ['title' => 'Actors', 'value' => $single['aktor']];
    $arr[] = ['title' => 'Rating', 'value' => $single['rating'] . '/' . '10'];
    $arr[] = ['title' => 'Duration', 'value' => $hour . ' hr ' . $min . ' min'];
    $arr[] = ['title' => 'Synopsis', 'value' => $single['deskripsi']];
    
    if(empty($single)){
        echo redirect('notfound.php');
    }
}

?>

<!-- Load parsing header -->
<?php load('templates/header.php') ?>
<!-- End load of header -->

<!-- Load parsing navbar -->
<?php load('templates/navbar.php') ?>
<!-- End load of navbar -->
<!-- single -->
<div class="single-page-agile-main">
    <div class="container">
        <!-- /w3l-medile-movies-grids -->
        <div class="agileits-single-top">
            <ol class="breadcrumb">
                <li><a href="">Movie</a></li>
                <li class="active">Single</li>
            </ol>
        </div>
        <div class="single-page-agile-info">
            <!-- /movie-browse-agile -->
            <div class="show-top-grids-w3lagile">
                <div class="col-sm-8 single-left">
                    <div class="song">
                        <div class="song-info">
                            <h3><?= $single['judulfilm'] ?> - Official Trailer</h3>
                        </div>
                        <div class="video-grid-single-page-agileits">
                            <iframe width="720" height="315" src="<?= $single['trailerfilm'] ?>">
                            </iframe>
                        </div>
                    </div>
                    <!-- Button trigger modal -->
                    <?php if(!empty($_SESSION['iduser'])):?>
                    <?php $r = get_where('rating_film',['rated_by' => $_SESSION['iduser'], 'idfilm' => $single['idfilm']], true)?>
                    <?php if(count($r) > 0):?>
                    <h5 style="margin-bottom: 20px;" class="text-success"><b>You gave <?= $r[0]['rating'] ?> / 10 for
                            this movie</b></h5>
                    <?php else:?>
                    <button style="margin-bottom: 20px;" type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#exampleModal">
                        Give rating
                    </button>
                    <?php endif;?>
                    <?php endif;?>
                    <?php foreach($arr as $val):?>
                    <h4 style="margin-top: 10px;"><?= $val['title'] ?></h4>
                    <p style="margin-top: 5px;"><?= $val['value'] ?></p>
                    <?php endforeach;?>
                    <div class="clearfix"> </div>

                    <div class="all-comments" style="margin-top: 100px;">
                        <h3>Reviews</h3>
                        <?php if(!empty($_SESSION['iduser'])):?>
                        <div class="all-comments-info">
                            <a href="#">Comments</a>
                            <div class="agile-info-wthree-box">
                                <form method="POST" action="<?= base_url('controller/post_comment.php') ?>">
                                    <textarea placeholder="Type your message ..." name="komentar" required=""
                                        class="form-control"></textarea>
                                    <input type="hidden" name="idfilm" value="<?= $single['idfilm'] ?>">
                                    <input type="hidden" name="act" value="post">
                                    <button class="btn btn-primary" type="submit">SEND</button>
                                    <div class="clearfix"> </div>
                                </form>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="media-grids">
                            <?php foreach($comment as $c):?>
                            <div class="media">
                                <h5><?= $c['nama'] ?></h5>
                                <div class="media-left">
                                    <a href="#">
                                        <img src="<?= base_url('assets/front/images/user.jpg') ?>"
                                            title="<?= $c['idfilm'] ?>" alt=" " />
                                    </a>
                                </div>
                                <div class="media-body">
                                    <p><?= $c['komentar'] ?></p>
                                    <span>Posted on : <?= $c['comment_at'] ?> </span>
                                    <?php if(!empty($_SESSION['iduser']) && ($_SESSION['iduser'] == $c['iduser'])):?>
                                    <form action="<?= base_url('controller/post_comment.php') ?>" method="POST">
                                        <input type="hidden" name="idreview" value="<?= $c['iduserreview'] ?>">
                                        <input type="hidden" name="act" value="del">
                                        <input type="hidden" name="idfilm" value="<?= $c['idfilm'] ?>">
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                    <?php endif;?>
                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 single-right">
                    <h3>Top Rating</h3>
                    <div class="single-grid-right">
                        <?php foreach($film as $f):?>
                        <div class="single-right-grids">
                            <div class="col-md-4 single-right-grid-left">
                                <a href="<?= base_url('single.php?id='. $f['idfilm']) ?>"><img
                                        src="<?= base_url('assets/front/images/' . $f['posterfilm']) ?>" alt="" /></a>
                            </div>
                            <div class="col-md-8 single-right-grid-right">
                                <a href="<?= base_url('single.php?id='. $f['idfilm']) ?>" class="title">
                                    <?= $f['judulfilm'] ?></a>
                                <p class="author"><a href="#" class="author"><?= $f['negara'] ?></a></p>
                                <p class="views"><?= $f['sutradara'] ?></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Give rating for this movie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('controller/movie_rating.php') ?>" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <select name="rating" id="rating" class="form-control">
                            <?php for($i = 1; $i <= 10; $i++):?>
                            <option value="<?= $i ?>"><?= $i . '/' . '10' ?></option>
                            <?php endfor;?>
                        </select>
                        <input type="hidden" name="idfilm" value="<?= $single['idfilm'] ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Load parsing header -->
<?php load('templates/footer.php') ?>
<!-- End load of header -->