<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <?php
    $setting = getSetting();
    $menu = getMenu();
    ?>
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('back_home.php') ?>">
        <div class="sidebar-brand-icon">
            <i class="fas fa-movie"></i>
        </div>
        <div class="sidebar-brand-text mx-2 mt-2"><?= $setting['app_name'] ?></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <?php
    foreach ($menu as $key => $val) {
        if (!empty($val['child'])) {
    ?>
    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#menu<?= $val['key'] ?>" aria-expanded="true"
            aria-controls="collapseMenu<?= $val['key'] ?>">
            <i class="fas fa-<?= $val['icon']; ?>"></i>
            <span><?= $val['name']; ?></span>
        </a>
        <div id="menu<?= $val['key'] ?>" class="collapse" aria-labelledby="headingPages"
            data-parent="#accordionSidebar">
            <div class="bg-white py-3 collapse-inner rounded">
                <!-- SIAPKAN SUBMENU MENU -->
                <?php foreach ($val['child'] as $keychild => $child) { ?>
                <!-- Nav Item - Dashboard -->
                <a class="collapse-item" href="<?= base_url($child['link']); ?>"><?= $child['name'] ?></a>
                <?php } ?>
            </div>
        </div>
        <!-- Divider -->
    </li>
    <?php
        } else {
        ?>
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url($val['link']) ?>">
            <i class="fas fa-<?= $val['icon'] ?>"></i>
            <span><?= $val['name'] ?></span>
        </a>
    </li>
    <?php
        }
        ?>
    <?php
    }
    ?>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->