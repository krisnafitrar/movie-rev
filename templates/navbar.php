<!-- nav -->
<div class="movies_nav">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav>
                    <ul class="nav navbar-nav">
                        <li class="<?= $_SESSION['nav_active'] == 'home' ? 'active' : ''; ?>"><a
                                href="<?= base_url('index.php') ?>">Home</a></li>
                        <li class="<?= $_SESSION['nav_active'] == 'movie' ? 'active' : ''; ?>"><a
                                href="<?= base_url('movie.php') ?>">Movie</a></li>
                        <li class="<?= $_SESSION['nav_active'] == 'about' ? 'active' : ''; ?>"><a
                                href="<?= base_url('about.php') ?>">About</a></li>
                        <?php if (!empty($_SESSION['username'])) : ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-user mr-2"
                                    style="margin-right: 10px;"></i><?= $_SESSION['username'] ?> <b
                                    class="caret"></b></a>
                            <ul class="dropdown-menu multi-column columns-3">
                                <li>
                                    <div class="col-sm-4">
                                        <ul class="multi-column-dropdown">
                                            <li><a href="<?= base_url('profile') ?>">Edit Profile</a></li>
                                            <li><a href="<?= base_url('wishlist.php') ?>">Wishlist
                                                    <span class="badge badge-danger"
                                                        style="margin-left: 3px;"><?= count(get_where('user_favorite',['iduser' => $_SESSION['iduser']],true)) ?>
                                                    </span></a>
                                            </li>
                                            <li><a href="<?= base_url('controller/auth_logout.php') ?>">Logout</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </li>
                        <?php else : ?>
                        <li><a href="<?= base_url('login.php') ?>">Login</a></li>
                        <?php endif; ?>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
</div>
<!-- //nav -->