<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= getTitle(); ?></title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="web movie review" />
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //for-mobile-apps -->
    <link href="<?= base_url('assets/front/css/bootstrap.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?= base_url('assets/front/css/style.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="<?= base_url('assets/front/css/contactstyle.css') ?>" type="text/css" media="all" />
    <link rel="stylesheet" href="<?= base_url('assets/front/css/faqstyle.css') ?>" type="text/css" media="all" />
    <link href="<?= base_url('assets/front/css/single.css') ?>" rel='stylesheet' type='text/css' />
    <link href="<?= base_url('assets/front/css/medile.css') ?>" rel='stylesheet' type='text/css' />
    <!-- banner-slider -->
    <link href="<?= base_url('assets/front/css/jquery.slidey.min.css') ?>" rel="stylesheet">
    <!-- //banner-slider -->
    <!-- pop-up -->
    <link href="<?= base_url('assets/front/css/popuo-box.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <!-- //pop-up -->
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="<?= base_url('assets/front/css/font-awesome.min.css') ?>" />
    <link href="<?= base_url('assets/fontawesome/css/all.css') ?>" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js -->
    <script type="text/javascript" src="<?= base_url('assets/front/js/jquery-2.1.4.min.js') ?>"></script>
    <!-- //js -->
    <!-- banner-bottom-plugin -->
    <link href="<?= base_url('assets/front/css/owl.carousel.css') ?>" rel="stylesheet" type="text/css" media="all">
    <script src="<?= base_url('assets/front/js/owl.carousel.js') ?>"></script>
    <script>
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({

                autoPlay: 3000, //Set AutoPlay to 3 seconds

                items: 5,
                itemsDesktop: [640, 4],
                itemsDesktopSmall: [414, 3]

            });

        });
    </script>
    <!-- //banner-bottom-plugin -->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="<?= base_url('assets/front/js/move-top.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/front/js/easing.js') ?>"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
</head>

<body>
    <!-- header -->
    <div class="header">
        <div class="container">
            <div class="w3layouts_logo">
                <a href="index.php">
                    <?php $app_name = explode(" ", getSetting()['app_name']); ?>
                    <h1><?= $app_name[0] ?><span><?= $app_name[1] ?></span></h1>
                </a>
            </div>
        </div>
    </div>
    <!-- //header -->