<?php

$title = "";

function insert($table, $record = [])
{
	global $conn;

	$query = "INSERT INTO $table ";

	$z = "";
	$x = count($record);
	$y = 0;

	foreach ($record as $key => $value) {
		$v = ($y == 0 ? "(" : "");
		$z .= $v . $key . ($x - $y > 1 ? ", " : "");
		$z .= ($x - $y == 1 ? ")" : "");
		$y += 1;
	}

	$query .= $z;

	$query .= " VALUES ";

	$y = 0;

	$z = "";

	foreach ($record as $key => $value) {
		$v = ($y == 0 ? "(" : "");
		$v .= gettype($value) != "string" ? $value : "'" . $value . "'";
		$z .= $v . ($x - $y > 1 ? ", " : "");
		$z .= ($x - $y == 1 ? ")" : "");
		$y += 1;
	}

	$query .= $z;

	if (mysqli_query($conn, $query)) {
		return true;
	} else {
		return false;
	}
}

function getData($table, $limit = null, $orderby = [])
{
	global $conn;

	$cond = "";

	$cond .= !empty($orderby) ? " ORDER BY " . $orderby['field'] . " " . $orderby['type'] : "";
	$cond .= !empty($limit) ? " LIMIT " . strval($limit) : "";

	$query = "SELECT * FROM $table" . $cond;
	$arr = array();
	$result = mysqli_query($conn, $query);

	while ($x_arr = mysqli_fetch_assoc($result)) {
		$arr[] = $x_arr;
	}

	return $arr;
}

function query($query,$is_list = false, $orderby = [])
{
	global $conn;

	$arr = array();
	$sort = " ORDER BY ";

	if(!empty($orderby)){
		foreach ($orderby as $key => $value) {
			$sort .= $key . " " . $value; 
		}

		$query .= $sort;
	}

	$result = mysqli_query($conn, $query);

	if ($is_list) {
		while ($x_arr = mysqli_fetch_assoc($result)) {
			$arr[] = $x_arr;
		}
	} else {
		while ($x_arr = mysqli_fetch_assoc($result)) {
			$arr = $x_arr;
		}
	}

	return $arr;
}


function get_where($table, $cond = [], $is_list = false)
{
	global $conn;

	$query = "SELECT * FROM $table WHERE ";
	$where = "";
	$x = count($cond);
	$y = 0;
	$arr = array();

	foreach ($cond as $key => $value) {
		$v = gettype($value) != "string" ? $value : "'" . $value . "'";
		$where .= $key . "=" . $v . ($x - $y > 1 ? " AND " : "");
		$y += 1;
	}

	$query .= $where;

	$result = mysqli_query($conn, $query);

	if ($is_list) {
		while ($val = mysqli_fetch_assoc($result)) {
			$arr[] = $val;
		}
	} else {
		while ($val = mysqli_fetch_assoc($result)) {
			$arr = $val;
		}
	}


	return $arr;
}


function update($table, $record = [], $where = [])
{
	global $conn;

	$query = "UPDATE $table SET ";
	$z = "";
	$cond = "";
	$x = count($record);
	$j = count($where);
	$y = 0;
	$a = 0;

	try {
		foreach ($record as $key => $value) {
			$v = gettype($value) != "string" ? $value : "'" . $value . "'";
			$z .= $key . "=" . $v . ($x - $y > 1 ? ", " : "");
			$y += 1;
		}

		foreach ($where as $key => $value) {
			$v = gettype($value) != "string" ? $value : "'" . $value . "'";
			$cond .= $key . "=" . $v . ($j - $a > 1 ? " AND " : "");
			$a += 1;
		}
	} catch (Exception $e) {
		echo $e;
	}

	$query .= $z . " WHERE " . $cond;

	if (mysqli_query($conn, $query)) {
		return true;
	} else {
		return false;
	}
}


function post($params)
{
	return $_POST[$params];
}


function get($params)
{
	return $_GET[$params];
}


function setFlashMessage($message, $status)
{
	session_start();
	$_SESSION['message'] = $message;
	$_SESSION['status'] = $status;
}

function getFlashMessage()
{
	if (isset($_SESSION['message']))
		return '<div class="alert alert-' . $_SESSION['status'] . ' alert-dismissible fade show" role="alert">' .
			$_SESSION['message'] . '
  		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    	<span aria-hidden="true">&times;</span>
  		</button>
		</div>';
	else
		return false;
}

function unsetFlashMessage()
{
	unset($_SESSION['message']);
}

function base_url($params)
{
	return "http://127.0.0.1/movie/" . $params;
}

function setTitle($t)
{
	$_SESSION['title'] = $t;
}

function getTitle()
{
	return $_SESSION['title'];
}

function load($view)
{
	include $view;
}

function redirect($url)
{
	return "<script>document.location.href='" . base_url($url) . "';</script>";
}

function getSetting()
{
	$arr = [];
	$setting = getData('setting');

	foreach ($setting as $val) {
		$arr[$val['idsetting']] = $val['value'];
	}

	return $arr;
}

function delete($table, $where = [])
{
	global $conn;

	$query = "DELETE FROM $table WHERE ";
	$cond = "";
	$j = count($where);
	$a = 0;

	try {
		foreach ($where as $key => $value) {
			$v = gettype($value) != "string" ? $value : "'" . $value . "'";
			$cond .= $key . "=" . $v . ($j - $a > 1 ? " AND " : "");
			$a += 1;
		}
	} catch (Exception $e) {
		print_r($e);
	}

	$query .= $cond;

	$result = mysqli_query($conn, $query);

	if ($result) {
		return true;
	} else {
		return false;
	}
}

function unset_session($record = [])
{
	for ($i = 0; $i < count($record); $i++) {
		unset($_SESSION[$record[$i]]);
	}
}

function getMenu()
{
    return [
        'home' => [
            'key' => 'home',
            'name' => 'Beranda',
            'link' => 'home',
            'icon' => 'home'
        ],
        'master' => [
            'key' => 'master',
            'name' => 'Master',
            'link' => '',
            'icon' => 'tasks',
            'child' => [
                'role' => [
                    'key' => 'role',
                    'name' => 'Role',
                    'link' => 'masters/role.php'
                ],
                'film' => [
                    'key' => 'film',
                    'name' => 'Data film',
                    'link' => 'masters/film.php'
                ],
                'genrefilm' => [
                    'key' => 'genrefilm',
                    'name' => 'Data Genre Film',
                    'link' => 'masters/genre.php'
                ],
                'users' => [
                    'key' => 'users',
                    'name' => 'Data User',
                    'link' => 'masters/users'
                ]
            ]
        ],
        'movie' => [
            'key' => 'movie',
            'name' => 'Movies',
            'link' => '',
            'icon' => 'book',
            'child' => [
                'moviesgenre' => [
                    'key' => 'moviesgenre',
                    'name' => 'List Genre',
                    'link' => 'movies/genre.php'
				],
				'moviesrating' => [
                    'key' => 'moviesrating',
                    'name' => 'Movies Rating',
                    'link' => 'movies/rating.php'
				],
				'userfav' => [
                    'key' => 'userfav',
                    'name' => 'User Favorite',
                    'link' => 'movies/userfav.php'
                ]
            ]
        ],
        'pengaturan' => [
            'key' => 'pengaturan',
            'name' => 'Pengaturan Aplikasi',
            'icon' => 'cogs',
            'link' => 'pengaturan'
        ],
        'logout' => [
            'key' => 'logout',
            'name' => 'Logout',
            'icon' => 'sign-out-alt',
            'link' => 'auth/logout'
        ]
    ];
}