<?php
include "conn.php";
include "functions/functions.php";
session_start();

setTitle('Register Page');

?>
<!-- load auth header -->
<?php include "templates/back/auth_header.php" ?>
<!-- end load auth header -->

<div class="container d-flex justify-content-center">

  <div class="card o-hidden border-0 shadow-lg my-5 col-md-7">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        <div class="col-lg-12">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4"><b>Buat akunmu disini</b></h1>
            </div>
            <?= (isset($_SESSION['message'])) ? getFlashMessage() : null; ?>
            <?php unset($_SESSION['message']); ?>
            <form class="user" method="POST" action="<?= base_url('controller/auth_registration.php') ?>">
              <div class="form-group">
                <input type="text" class="form-control form-control-user" name="nama" id="exampleFirstName" placeholder="Nama Lengkap" required>
              </div>
              <div class="form-group">
                <input type="text" class="form-control form-control-user" name="username" id="exampleInputEmail" placeholder="Username" required>
              </div>
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                  <input type="password" class="form-control form-control-user" name="password1" id="exampleInputPassword" placeholder="Password" required>
                </div>
                <div class="col-sm-6">
                  <input type="password" class="form-control form-control-user" name="password2" id="exampleRepeatPassword" placeholder="Ulangi Password" req>
                </div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control form-control-user" name="email" id="exampleInputEmail" placeholder="Email Address" required>
              </div>
              <button type="submit" class="btn btn-primary btn-user btn-block">
                Daftar akun
              </button>
              <hr>
            </form>
            <hr>
            <div class="text-center">
              <a class="small" href="<?= base_url('login.php') ?>">Sudah punya akun? Login!</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<!-- load auth header -->
<?php include "templates/back/auth_footer.php" ?>
<!-- end load auth header -->