<?php
include "conn.php";
include "functions/functions.php";
session_start();

setTitle('Login Page');

if (isset($_SESSION['username'])) {
  if ($_SESSION['iduser'] > 1) {
    echo redirect('index.php');
  } else {
    echo redirect('home.php');
  }
}


?>
<!-- load auth header -->
<?php include "templates/back/auth_header.php" ?>
<!-- end load auth header -->

<div class="container">

  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-xl-5 col-lg-5 col-md-5">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4"><b>Login Movie</b></h1>
                </div>
                <?= (isset($_SESSION['message'])) ? getFlashMessage() : null; ?>
                <?php unset($_SESSION['message']); ?>
                <form class="user" method="POST" action="<?= base_url('controller/auth_login.php') ?>">
                  <div class="form-group">
                    <input type="text" class="form-control form-control-user" id="exampleInputEmail" name="username" aria-describedby="emailHelp" placeholder="Masukkan username" required>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control form-control-user" name="password" id="exampleInputPassword" placeholder="Password" required>
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">
                    Login
                  </button>
                  <hr>
                </form>
                <hr>
                <div class="text-center">
                  <a class="small" href="<?= base_url('index.php') ?>">Kembali ke home</a>
                </div>
                <div class="text-center">
                  <a class="small" href="register.php">Buat akun</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>

<!-- load auth header -->
<?php include "templates/back/auth_footer.php" ?>
<!-- end load auth header -->