<?php
session_start();

include "conn.php";
include "functions/functions.php";

$title = setTitle('Home Page | ' . getSetting()['app_name']);
$_SESSION['genre'] = getData('genre_film');
$_SESSION['nav_active'] = 'home';

function getGenre($params)
{
    return query("SELECT * FROM list_genre_film JOIN genre_film USING(idgenre) WHERE idfilm='$params'", true);
}

?>

<!-- Load parsing header -->
<?php load('templates/header.php') ?>
<!-- End load of header -->

<!-- bootstrap-pop-up -->
<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Sign In & Sign Up
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <section>
                <div class="modal-body">
                    <div class="w3_login_module">
                        <div class="module form-module">
                            <div class="toggle"><i class="fa fa-times fa-pencil"></i>
                                <div class="tooltip">Click Me</div>
                            </div>
                            <div class="form">
                                <h3>Login to your account</h3>
                                <form action="#" method="post">
                                    <input type="text" name="Username" placeholder="Username" required="">
                                    <input type="password" name="Password" placeholder="Password" required="">
                                    <input type="submit" value="Login">
                                </form>
                            </div>
                            <div class="form">
                                <h3>Create an account</h3>
                                <form action="#" method="post">
                                    <input type="text" name="Username" placeholder="Username" required="">
                                    <input type="password" name="Password" placeholder="Password" required="">
                                    <input type="email" name="Email" placeholder="Email Address" required="">
                                    <input type="text" name="Phone" placeholder="Phone Number" required="">
                                    <input type="submit" value="Register">
                                </form>
                            </div>
                            <div class="cta"><a href="#">Forgot your password?</a></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<script>
    $('.toggle').click(function () {
        // Switches the Icon
        $(this).children('i').toggleClass('fa-pencil');
        // Switches the forms  
        $('.form').animate({
            height: "toggle",
            'padding-top': 'toggle',
            'padding-bottom': 'toggle',
            opacity: "toggle"
        }, "slow");
    });
</script>
<!-- //bootstrap-pop-up -->

<!-- Load parsing navbar -->
<?php load('templates/navbar.php') ?>
<!-- End load of navbar -->

<!-- banner -->
<div id="slidey" style="display:none;">
    <ul>
        <?php foreach (getData('film', 5, ['field' => 'insert_at', 'type' => 'DESC']) as $film) : ?>
        <li><img src="<?= base_url('assets/front/images/' . $film['jumbotron']) ?>" alt=" ">
            <p class='title'><?= $film['judulfilm'] ?></p>
            <p class='description'><?= $film['deskripsi'] ?></p>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<script src="<?= base_url('assets/front/js/jquery.slidey.js') ?>"></script>
<script src="<?= base_url('assets/front/js/jquery.dotdotdot.min.js') ?>"></script>
<script type="text/javascript">
    $("#slidey").slidey({
        interval: 8000,
        listCount: 5,
        autoplay: false,
        showList: true
    });
    $(".slidey-list-description").dotdotdot();
</script>

<!-- general -->
<div class="general">
    <h4 class="latest-text w3_latest_text">Featured Movies</h4>
    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab"
                        aria-controls="home" aria-expanded="true">Featured</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                    <div class="mt-1">
                        <h3>Result : <?= count(getData('film', 12)) ?> Movies</h3>
                    </div>
                    <br>
                    <div class="w3_agile_featured_movies mt-3">
                        <?php foreach (getData('film', 12, ['field' => 'insert_at', 'type' => 'DESC']) as $film) : ?>
                        <div class="col-md-2 w3l-movie-gride-agile">
                            <a href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"
                                class="hvr-shutter-out-horizontal"><img
                                    src="<?= base_url('assets/front/images/' . $film['posterfilm']) ?>"
                                    title="album-name" class="img-responsive" alt=" " />
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i>
                                </div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a
                                            href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"><b><?= $film['judulfilm'] ?></b></a>
                                    </h6>
                                </div>
                                <div class="mt-2">
                                    <p><?php
                                            $g = getGenre($film['idfilm'], true);
                                            $i = 0;
                                            if (count($g) > 1) {
                                                foreach ($g as $val) {
                                                    $slash = (count($g) - $i > 1 ? " / " : "");
                                                    echo $val['genrefilm'] . $slash;
                                                    $i += 1;
                                                }
                                            } else {
                                                echo $g[0]['genrefilm'];
                                            }
                                            ?></p>
                                </div>
                                <div class="mt-2">
                                    <?php if (!empty($_SESSION['username'])) : ?>
                                    <?php $wishlist = get_where('user_favorite', ['iduser' => $_SESSION['iduser'], 'idfilm' => $film['idfilm']]); ?>
                                    <button type="button" onclick="sendData(this)"
                                        class="btn btn-block btn-<?= empty($wishlist) ? "primary" : "secondary" ?>"
                                        data-id="<?= $film['idfilm'] ?>" data-page="index"
                                        data-action="<?= empty($wishlist) ? "add" : "remove" ?>"><?= empty($wishlist) ? "Add to wishlist" : "Remove from wishlist" ?></button>
                                    <?php else : ?>
                                    <a href="<?= base_url('login.php') ?>" class="btn btn-block btn-primary">Add to
                                        wishlist</a>
                                    <?php endif; ?>
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><?= $film['tahunrilis'] ?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
                                            <?php for ($i = 0; $i < floor($film['rating'] / 2); $i++) : ?>
                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                            <?php endfor; ?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        </div>
        <h3 class="text-center"><a href="<?= base_url('movie.php') ?>">Load all movies</a></h3>
    </div>
</div>

<!-- top movies -->
<div class="general">
    <h4 class="latest-text w3_latest_text">Top Rating Movies</h4>
    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#rating" id="rating-tab" role="tab" data-toggle="tab"
                        aria-controls="rating" aria-expanded="true">Top Rating</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                    <div class="mt-1">
                        <h3>Result : <?= count(getData('film', 3)) ?> Movies</h3>
                    </div>
                    <br>
                    <div class="w3_agile_featured_movies mt-3">
                        <?php foreach (getData('film', 3, ['field' => 'rating', 'type' => 'DESC']) as $film) : ?>
                        <div class="col-md-2 w3l-movie-gride-agile">
                            <a href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"
                                class="hvr-shutter-out-horizontal"><img
                                    src="<?= base_url('assets/front/images/' . $film['posterfilm']) ?>"
                                    title="album-name" class="img-responsive" alt=" " />
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i>
                                </div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a
                                            href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"><b><?= $film['judulfilm'] ?></b></a>
                                    </h6>
                                </div>
                                <div class="mt-2">
                                    <p><?php
                                            $g = getGenre($film['idfilm'], true);
                                            $i = 0;
                                            if (count($g) > 1) {
                                                foreach ($g as $val) {
                                                    $slash = (count($g) - $i > 1 ? " / " : "");
                                                    echo $val['genrefilm'] . $slash;
                                                    $i += 1;
                                                }
                                            } else {
                                                echo $g[0]['genrefilm'];
                                            }
                                            ?></p>
                                </div>
                                <div class="mt-2">
                                    <?php if (!empty($_SESSION['username'])) : ?>
                                    <?php $wishlist = get_where('user_favorite', ['iduser' => $_SESSION['iduser'], 'idfilm' => $film['idfilm']]); ?>
                                    <button type="button" id="btnfav"
                                        class="btn btn-block btn-<?= empty($wishlist) ? "primary" : "secondary" ?>"
                                        data-id="<?= $film['idfilm'] ?>" data-page="index" onclick="sendData(this)"
                                        data-action="<?= empty($wishlist) ? "add" : "remove" ?>"><?= empty($wishlist) ? "Add to wishlist" : "Remove from wishlist" ?></button>
                                    <?php else : ?>
                                    <a href="<?= base_url('login.php') ?>" class="btn btn-block btn-primary">Add to
                                        wishlist</a>
                                    <?php endif; ?>
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><?= $film['tahunrilis'] ?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
                                            <?php for ($i = 0; $i < floor($film['rating'] / 2); $i++) : ?>
                                            <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                            <?php endfor; ?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- pop-up-box -->
<script src="<?= base_url('assets/front/js/jquery.magnific-popup.js') ?>" type="text/javascript"></script>
<!--//pop-up-box -->
<div id="small-dialog" class="mfp-hide">
    <iframe src="https://player.vimeo.com/video/164819130?title=0&byline=0"></iframe>
</div>
<div id="small-dialog1" class="mfp-hide">
    <iframe src="https://player.vimeo.com/video/148284736"></iframe>
</div>
<div id="small-dialog2" class="mfp-hide">
    <iframe src="https://player.vimeo.com/video/165197924?color=ffffff&title=0&byline=0&portrait=0"></iframe>
</div>
<script>
    $(document).ready(function () {
        $('.w3_play_icon,.w3_play_icon1,.w3_play_icon2').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });

    });
</script>
<!-- //Latest-tv-series -->

<?php load('templates/footer.php') ?>