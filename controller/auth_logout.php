<?php

include "../functions/functions.php";

session_start();

$sess = ['iduser', 'username', 'nama', 'idrole', 'email'];

unset_session($sess);

echo redirect('index.php');
