<?php

include "../conn.php";
include "../functions/functions.php";

$username = post('username');
$password = post('password');

if (!empty($username)) {
    if (!empty($password)) {
        $user = get_where('users', ['username' => $username]);
        if ($user) {
            if (password_verify($password, $user['password'])) {
                session_start();
                $_SESSION['iduser'] = $user['iduser'];
                $_SESSION['username'] = $user['username'];
                $_SESSION['nama'] = $user['nama'];
                $_SESSION['idrole'] = $user['idrole'];
                $_SESSION['email'] = $user['email'];

                if ($user['idrole'] > 1) {
                    echo redirect('index.php');
                } else {
                    echo redirect('back_home.php');
                }
            } else {
                setFlashMessage('Password salah!', 'danger');
                echo redirect('login.php');
            }
        } else {
            setFlashMessage('User tidak terdaftar!', 'danger');
            echo redirect('login.php');
        }
    } else {
        setFlashMessage('Masukkan password!', 'danger');
        echo redirect('login.php');
    }
} else {
    setFlashMessage('Masukkan username!', 'danger');
    echo redirect('login.php');
}