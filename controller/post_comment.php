<?php
session_start();

include "../conn.php";
include "../functions/functions.php";

$act = post('act');

$iduser = $_SESSION['iduser'];
$idfilm = post('idfilm');

if($act == "post"){
    $komentar = post('komentar');

    if(!empty($iduser) && !empty($idfilm) && 
    !empty($komentar)){
        $ins = insert('user_review',[
            'idfilm' => $idfilm,
            'iduser' => $iduser,
            'komentar' => $komentar
        ]);

        if($ins){
            echo redirect('single.php?id=' . $idfilm);            
        }else{
            echo redirect('single.php?id=' . $idfilm);
        }   
    }else{
        echo redirect('error.php');
    }
}else{
    $idreview = post('idreview');
    delete('user_review',['iduserreview' => $idreview]);
    echo redirect('single.php?id=' . $idfilm);
}