<?php

include "../conn.php";
include "../functions/functions.php";

$iduser = post('iduser');
$idfilm = post('idfilm');
$act = post('act');

$data = [
    'iduser' => $iduser,
    'idfilm' => $idfilm
];

switch ($act) {
    case 'add':
        insert('user_favorite', $data);
        break;
    case 'remove':
        delete('user_favorite', $data);
        break;
}
