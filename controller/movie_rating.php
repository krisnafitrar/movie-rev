<?php
session_start();

include "../conn.php";
include "../functions/functions.php";

$rating = post('rating');
$iduser = $_SESSION['iduser'];
$idfilm = post('idfilm');

$person = count(get_where('rating_film', ['idfilm' => $idfilm], true)) + 1;
$current_rating = get_where('film',['idfilm' => $idfilm])['rating'] + $rating;
$result = number_format($current_rating / $person,1,'.','');

//insert to tabel rating_film
$ins = insert('rating_film',[
    'idfilm' => $idfilm,
    'rated_by' => $iduser,
    'rating' => $rating
]);

//lalu update table film nya
$up = update('film',[
    'rating' => $result
],['idfilm' => $idfilm]);

if($ins && $up){
    echo redirect('single.php?id=' . $idfilm);
}else{
    echo redirect('error.php');
}