<?php

include "../conn.php";
include "../functions/functions.php";


$nama = htmlspecialchars(post('nama'));
$username = htmlspecialchars(post('username'));
$password1 = post('password1');
$password2 = post('password2');
$email = htmlspecialchars(post('email'));
$idrole = 2;
$is_active = 1;

if (!empty($nama) && !empty($username) && !empty($password1) && !empty($email)) {
    if (!get_where('users', ['username' => $username])) {
        if ($password1 == $password2) {
            $a_data = [
                'nama' => $nama,
                'username' => $username,
                'password' => password_hash($password1, PASSWORD_DEFAULT),
                'email' => $email,
                'idrole' => $idrole,
                'is_active' => $is_active,
            ];

            $ok = insert('users', $a_data);

            if ($ok) {
                setFlashMessage('Berhasil membuat akun', 'success');
                echo redirect('register.php');
            } else {
                setFlashMessage('Gagal membuat akun!', 'danger');
                echo redirect('register.php');
            }
        } else {
            setFlashMessage('Password tidak cocok!', 'danger');
            echo redirect('register.php');
        }
    } else {
        setFlashMessage('Username sudah digunakan!', 'danger');
        echo redirect('register.php');
    }
} else {
    setFlashMessage('Mohon lengkapi semua data!', 'danger');
    echo redirect('register.php');
}
