-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Jun 2020 pada 14.53
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moviereview`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `film`
--

CREATE TABLE `film` (
  `idfilm` varchar(32) NOT NULL,
  `judulfilm` varchar(255) NOT NULL,
  `sutradara` varchar(128) NOT NULL,
  `negara` varchar(128) NOT NULL,
  `aktor` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `posterfilm` varchar(255) NOT NULL,
  `jumbotron` varchar(255) DEFAULT NULL,
  `trailerfilm` varchar(255) NOT NULL,
  `durasi` int(11) NOT NULL,
  `tahunrilis` varchar(16) NOT NULL,
  `rating` varchar(5) NOT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `insert_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `film`
--

INSERT INTO `film` (`idfilm`, `judulfilm`, `sutradara`, `negara`, `aktor`, `deskripsi`, `posterfilm`, `jumbotron`, `trailerfilm`, `durasi`, `tahunrilis`, `rating`, `insert_at`, `insert_by`) VALUES
('MV0001', 'Tarzan', 'Richard', 'USA', 'lorem,ipsum,dolor', 'Tarzan, having acclimated to life in London, is called back to his former home\r\nin the jungle to investigate the activities at a mining encampment.', 'tarzanposter.jpg', 'tarzan.jpg', 'https://www.youtube.com/embed/tgbNymZ7vqY', 125, '2010', '7', '2020-06-09 11:23:15', 1),
('MV0002', 'Maximum Ride', 'Lorem Ipsum', 'USA', 'lorem,ipsum,dolor', 'Six children, genetically cross-bred with avian DNA, take flight around the country to discover their origins. Along the way, their mysterious past is.', 'film1.jpg', 'maxridejumbotron.jpg', 'https://www.youtube.com/embed/tgbNymZ7vqY', 135, '2011', '7', '2020-06-09 11:23:19', 1),
('MV0003', 'Independence', 'Lorem Ipsum', 'Inggris', 'lorem,ipsum,dolor', 'The fate of humanity hangs in the balance as the U.S. President and citizens decide if these aliens are to be trusted ...or feared.', 'independencenew.jpg', 'jumbotron_independence.jpg', 'https://www.youtube.com/embed/tgbNymZ7vqY', 123, '2016', '4', '2020-06-09 11:23:23', 1),
('MV0004', 'The Counjuring 2', 'Michael', 'USA', 'Lorem, ipsum, dolor', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe dolores optio necessitatibus illum natus consequatur commodi quasi ipsa, nam tempora modi, repellat recusandae aliquid nostrum possimus? Cum vitae sit quod.', 'counjuringnew.jpg', 'jumbotron_counjuring.jpg', 'https://www.youtube.com/embed/tgbNymZ7vqY', 120, '2017', '7.5', '2020-06-09 11:23:27', 1),
('MV0005', 'Central Intellegence', 'Richard', 'USA', 'lorem, ipsum, dolor', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias ipsa accusantium suscipit repudiandae sit similique esse aspernatur soluta magnam eaque aliquid, laboriosam aliquam asperiores, voluptate qui saepe assumenda tenetur ea.', 'central.jpg', 'jumbotron_central.jpg', 'https://www.youtube.com/embed/tgbNymZ7vqY', 130, '2018', '7.0', '2020-06-09 11:57:29', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `genre_film`
--

CREATE TABLE `genre_film` (
  `idgenre` int(11) NOT NULL,
  `genrefilm` varchar(128) NOT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `genre_film`
--

INSERT INTO `genre_film` (`idgenre`, `genrefilm`, `info_tambahan`) VALUES
(1, 'Romance', NULL),
(2, 'Horror', NULL),
(3, 'Thriller', NULL),
(4, 'Comedy', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_genre_film`
--

CREATE TABLE `list_genre_film` (
  `idlistgenre` int(11) NOT NULL,
  `idfilm` varchar(32) NOT NULL,
  `idgenre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `list_genre_film`
--

INSERT INTO `list_genre_film` (`idlistgenre`, `idfilm`, `idgenre`) VALUES
(1, 'MV0001', 3),
(2, 'MV0001', 4),
(3, 'MV0002', 4),
(4, 'MV0003', 2),
(5, 'MV0004', 3),
(6, 'MV0004', 2),
(7, 'MV0005', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating_film`
--

CREATE TABLE `rating_film` (
  `idrating` int(11) NOT NULL,
  `idfilm` varchar(32) NOT NULL,
  `rated_by` int(1) NOT NULL,
  `rating` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rating_film`
--

INSERT INTO `rating_film` (`idrating`, `idfilm`, `rated_by`, `rating`, `time`) VALUES
(1, 'MV0001', 1, 8, '2020-06-03 08:51:01'),
(2, 'MV0002', 1, 7, '2020-06-03 08:51:01'),
(3, 'MV0003', 1, 6, '2020-06-03 08:51:27'),
(4, 'MV0004', 1, 7, '2020-06-03 08:51:27'),
(5, 'MV0005', 1, 9, '2020-06-03 08:51:40'),
(6, 'MV0001', 2, 6, '2020-06-03 11:37:11'),
(7, 'MV0002', 2, 7, '2020-06-03 11:37:11'),
(8, 'MV0003', 2, 3, '2020-06-03 11:37:11'),
(9, 'MV0004', 2, 8, '2020-06-03 11:37:11'),
(12, 'MV0005', 2, 5, '2020-06-09 11:57:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE `setting` (
  `idsetting` varchar(64) NOT NULL,
  `namasetting` varchar(128) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`idsetting`, `namasetting`, `value`) VALUES
('app_name', 'Nama Aplikasi', 'Nama Aplikasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `idrole` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`iduser`, `nama`, `username`, `password`, `email`, `idrole`, `is_active`, `create_at`) VALUES
(1, 'krisna fitra', 'krisnafitrar', '$2y$10$5nWu1./vubrS9Rgorjk6mO.YJjChGdzwokO8eVqGgO/5H/Yx7EzQu', 'krisnafffr@gmail.com', 2, 1, '2020-06-02 13:24:28'),
(2, 'User Coba', 'cobauser', '$2y$10$iwRhYj5rkYLE8C0I93hvD.lX8OiV3b7v4LvmWpSuWbDfClcSQKr4.', 'cobauser@gmail.com', 2, 1, '2020-06-03 08:52:53'),
(3, 'Administrator', 'admin', '$2y$10$iwRhYj5rkYLE8C0I93hvD.lX8OiV3b7v4LvmWpSuWbDfClcSQKr4.', 'admin@gmail.com', 1, 1, '2020-06-09 12:47:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_favorite`
--

CREATE TABLE `user_favorite` (
  `iduserfav` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idfilm` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_favorite`
--

INSERT INTO `user_favorite` (`iduserfav`, `iduser`, `idfilm`) VALUES
(4, 1, 'MV0001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_review`
--

CREATE TABLE `user_review` (
  `iduserreview` int(11) NOT NULL,
  `idfilm` varchar(32) NOT NULL,
  `iduser` int(11) NOT NULL,
  `komentar` text DEFAULT NULL,
  `comment_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_review`
--

INSERT INTO `user_review` (`iduserreview`, `idfilm`, `iduser`, `komentar`, `comment_at`) VALUES
(1, 'MV0003', 2, 'kereen', '2020-06-09 01:01:08'),
(2, 'MV0003', 2, 'mantuuul', '2020-06-09 01:02:29'),
(4, 'MV0003', 2, 'woy', '2020-06-09 01:04:09'),
(8, 'MV0005', 2, 'oyi', '2020-06-09 11:25:39'),
(9, 'MV0005', 2, 'aku sudah merating film ini lho.. aku kasih rating 5', '2020-06-09 11:58:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `idrole` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`idrole`, `role`) VALUES
(1, 'Administrator'),
(2, 'User');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`idfilm`),
  ADD UNIQUE KEY `idfilm` (`idfilm`),
  ADD KEY `insert_by` (`insert_by`);

--
-- Indeks untuk tabel `genre_film`
--
ALTER TABLE `genre_film`
  ADD PRIMARY KEY (`idgenre`);

--
-- Indeks untuk tabel `list_genre_film`
--
ALTER TABLE `list_genre_film`
  ADD PRIMARY KEY (`idlistgenre`),
  ADD KEY `idfilm` (`idfilm`),
  ADD KEY `idgenre` (`idgenre`);

--
-- Indeks untuk tabel `rating_film`
--
ALTER TABLE `rating_film`
  ADD PRIMARY KEY (`idrating`),
  ADD KEY `idfilm` (`idfilm`),
  ADD KEY `rated_by` (`rated_by`);

--
-- Indeks untuk tabel `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`idsetting`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `idrole` (`idrole`);

--
-- Indeks untuk tabel `user_favorite`
--
ALTER TABLE `user_favorite`
  ADD PRIMARY KEY (`iduserfav`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `idfilm` (`idfilm`);

--
-- Indeks untuk tabel `user_review`
--
ALTER TABLE `user_review`
  ADD PRIMARY KEY (`iduserreview`),
  ADD KEY `idfilm` (`idfilm`),
  ADD KEY `iduser` (`iduser`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`idrole`),
  ADD UNIQUE KEY `role` (`role`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `genre_film`
--
ALTER TABLE `genre_film`
  MODIFY `idgenre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `list_genre_film`
--
ALTER TABLE `list_genre_film`
  MODIFY `idlistgenre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `rating_film`
--
ALTER TABLE `rating_film`
  MODIFY `idrating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user_favorite`
--
ALTER TABLE `user_favorite`
  MODIFY `iduserfav` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `user_review`
--
ALTER TABLE `user_review`
  MODIFY `iduserreview` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `idrole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `list_genre_film`
--
ALTER TABLE `list_genre_film`
  ADD CONSTRAINT `list_genre_film_ibfk_1` FOREIGN KEY (`idgenre`) REFERENCES `genre_film` (`idgenre`),
  ADD CONSTRAINT `list_genre_film_ibfk_2` FOREIGN KEY (`idfilm`) REFERENCES `film` (`idfilm`);

--
-- Ketidakleluasaan untuk tabel `rating_film`
--
ALTER TABLE `rating_film`
  ADD CONSTRAINT `rating_film_ibfk_1` FOREIGN KEY (`idfilm`) REFERENCES `film` (`idfilm`),
  ADD CONSTRAINT `rating_film_ibfk_2` FOREIGN KEY (`rated_by`) REFERENCES `users` (`iduser`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`idrole`) REFERENCES `user_role` (`idrole`);

--
-- Ketidakleluasaan untuk tabel `user_favorite`
--
ALTER TABLE `user_favorite`
  ADD CONSTRAINT `user_favorite_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`),
  ADD CONSTRAINT `user_favorite_ibfk_2` FOREIGN KEY (`idfilm`) REFERENCES `film` (`idfilm`);

--
-- Ketidakleluasaan untuk tabel `user_review`
--
ALTER TABLE `user_review`
  ADD CONSTRAINT `user_review_ibfk_1` FOREIGN KEY (`idfilm`) REFERENCES `film` (`idfilm`),
  ADD CONSTRAINT `user_review_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `users` (`iduser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
