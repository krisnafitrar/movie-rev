<?php
session_start();

include "conn.php";
include "functions/functions.php";

$title = setTitle('All Movies | ' . getSetting()['app_name']);
$_SESSION['genre'] = getData('genre_film');
$_SESSION['nav_active'] = 'movie';

$curr_nav = 0;

if (!empty($_POST['keyword']) && empty($_GET['genre'])) {
    $keyword = $_POST['keyword'];
    $result = query("SELECT * FROM film WHERE judulfilm LIKE '%$keyword%' ORDER BY insert_at DESC", true);
} else if ((isset($_GET['genre']) && empty($_POST['keyword']))) {
    $curr_nav = $_GET['genre'];
    $result = query("SELECT * FROM list_genre_film JOIN film USING(idfilm) WHERE idgenre=$curr_nav", true);
} else if (!empty($_POST['keyword']) && !empty($_GET['genre'])) {
    $curr_nav = $_GET['genre'];
    $keyword = $_POST['keyword'];
    $result = query("SELECT * FROM list_genre_film ls JOIN film f ON ls.idfilm=f.idfilm WHERE ls.idgenre=$curr_nav AND f.judulfilm LIKE '%$keyword%'", true);
} else {
    $result = getData('film', null, ['field' => 'insert_at', 'type' => 'DESC']);
}


function getGenre($params)
{
    return query("SELECT * FROM list_genre_film JOIN genre_film USING(idgenre) WHERE idfilm='$params'", true);
}
?>

<!-- Load parsing header -->
<?php load('templates/header.php') ?>
<!-- End load of header -->

<!-- Load parsing navbar -->
<?php load('templates/navbar.php') ?>
<!-- End load of navbar -->

<div class="wthree-comedy">
    <!-- /w3l-medile-movies-grids -->
    <div class="w3l-medile-movies-grids">
        <!-- /movie-browse-agile -->
        <div class="movie-browse-agile">
            <!--/browse-agile-w3ls -->
            <div class="browse-agile-w3ls general-w3ls">
                <div class="tittle-head">
                    <h4 class="latest-text">All Movies</h4>
                    <div class="container">
                        <div class="agileits-single-top">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Movies</a></li>
                                <li class="active">All Movies</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <form method="POST">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="keyword"
                                        placeholder="Type a keyword ..." style="margin-left: -15px;">
                                </div>
                                <div class="col-sm">
                                    <button type="submit" class="btn btn-primary"
                                        style="margin-left: -15px;">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br></br>
                    <ul class="nav nav-pills" style="margin-bottom: 20px;">
                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url('movie.php') ?>">
                                <div class="text-<?= $curr_nav == 0 ? 'warning' : 'primary' ?>">
                                    <?= $curr_nav == 0 ? '<b>All</b>' : 'All' ?></div>
                            </a>
                        </li>
                        <?php foreach (getData('genre_film') as $g) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url('movie.php?genre=' . $g['idgenre']) ?>">
                                <div class="text-<?= $curr_nav == $g['idgenre'] ? 'warning' : 'primary' ?>">
                                    <?= $curr_nav == $g['idgenre'] ? '<b>' . $g['genrefilm'] . '</b>' : $g['genrefilm'] ?>
                                </div>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php if (empty($result) && !empty($keyword)) : ?>
                    <!-- cek apakah array null or non null if null tampilkan pesan -->
                    <h3 class="text-danger">Search results for "<?= $keyword; ?>" : <?= count($result) ?>, No movies
                        were found matching your selection!</h3>
                    <?php elseif (empty($result)) : ?>
                    <h3 class="text-danger">Data not found</h3>
                    <?php endif; ?>
                    <div class="browse-inner-come-agile-w3">
                        <?php foreach ($result as $film) : ?>
                        <div class="w3_agile_featured_movies mt-3">
                            <div class="col-md-2 w3l-movie-gride-agile">
                                <a href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"
                                    class="hvr-shutter-out-horizontal"><img
                                        src="<?= base_url('assets/front/images/' . $film['posterfilm']) ?>"
                                        title="album-name" class="img-responsive" alt=" " />
                                    <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i>
                                    </div>
                                </a>
                                <div class="mid-1 agileits_w3layouts_mid_1_home">
                                    <div class="w3l-movie-text">
                                        <h6><a
                                                href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"><b><?= $film['judulfilm'] ?></b></a>
                                        </h6>
                                    </div>
                                    <div class="mt-2">
                                        <p><?php
                                                $g = getGenre($film['idfilm'], true);
                                                $i = 0;
                                                if (count($g) > 1) {
                                                    foreach ($g as $val) {
                                                        $slash = (count($g) - $i > 1 ? " / " : "");
                                                        echo $val['genrefilm'] . $slash;
                                                        $i += 1;
                                                    }
                                                } else {
                                                    echo $g[0]['genrefilm'];
                                                }
                                                ?></p>
                                    </div>
                                    <div class="mt-2">
                                        <?php if (!empty($_SESSION['username'])) : ?>
                                        <?php $wishlist = get_where('user_favorite', ['iduser' => $_SESSION['iduser'], 'idfilm' => $film['idfilm']]); ?>
                                        <button type="button" onclick="sendData(this)"
                                            class="btn btn-block btn-<?= empty($wishlist) ? "primary" : "secondary" ?>"
                                            data-id="<?= $film['idfilm'] ?>" data-page="movie"
                                            data-action="<?= empty($wishlist) ? "add" : "remove" ?>"><?= empty($wishlist) ? "Add to wishlist" : "Remove from wishlist" ?></button>
                                        <?php else : ?>
                                        <a href="<?= base_url('login.php') ?>" class="btn btn-block btn-primary">Add to
                                            wishlist</a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="mid-2 agile_mid_2_home">
                                        <p><?= $film['tahunrilis'] ?></p>
                                        <div class="block-stars">
                                            <ul class="w3l-ratings">
                                                <?php for ($i = 0; $i < floor($film['rating'] / 2); $i++) : ?>
                                                <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                <?php endfor; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- //movie-browse-agile -->

    </div>
    <!-- //w3l-medile-movies-grids -->
</div>

<!-- Load parsing header -->
<?php load('templates/footer.php') ?>
<!-- End load of header -->