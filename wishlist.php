<?php
session_start();

if(empty($_SESSION['iduser'])){
    echo redirect('error.php');
}

include "conn.php";
include "functions/functions.php";

$title = setTitle('All Movies | ' . getSetting()['app_name']);
$_SESSION['nav_active'] = 'movie';

$curr_nav = 0;
$iduser = $_SESSION['iduser'];

$result = query("SELECT * FROM user_favorite JOIN film USING(idfilm) WHERE iduser=$iduser", true);


function getGenre($params)
{
    return query("SELECT * FROM list_genre_film JOIN genre_film USING(idgenre) WHERE idfilm='$params'", true);
}
?>

<!-- Load parsing header -->
<?php load('templates/header.php') ?>
<!-- End load of header -->

<!-- Load parsing navbar -->
<?php load('templates/navbar.php') ?>
<!-- End load of navbar -->

<div class="wthree-comedy">
    <!-- /w3l-medile-movies-grids -->
    <div class="w3l-medile-movies-grids">
        <!-- /movie-browse-agile -->
        <div class="movie-browse-agile">
            <!--/browse-agile-w3ls -->
            <div class="browse-agile-w3ls general-w3ls">
                <div class="tittle-head">
                    <h4 class="latest-text">Watch Later</h4>
                    <div class="container">
                        <div class="agileits-single-top">
                            <ol class="breadcrumb">
                                <li><a href="index.html">Movies</a></li>
                                <li class="active">Wishlist</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="browse-inner-come-agile-w3">
                        <?php if(empty($result)):?>
                        <h3 class="text-danger text-center">Wishlist is empty</h3>
                        <?php endif;?>
                        <?php foreach ($result as $film) : ?>
                        <div class="w3_agile_featured_movies mt-3">
                            <div class="col-md-2 w3l-movie-gride-agile">
                                <a href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"
                                    class="hvr-shutter-out-horizontal"><img
                                        src="<?= base_url('assets/front/images/' . $film['posterfilm']) ?>"
                                        title="album-name" class="img-responsive" alt=" " />
                                    <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i>
                                    </div>
                                </a>
                                <div class="mid-1 agileits_w3layouts_mid_1_home">
                                    <div class="w3l-movie-text">
                                        <h6><a
                                                href="<?= base_url('single.php?id=' . $film['idfilm']) ?>"><b><?= $film['judulfilm'] ?></b></a>
                                        </h6>
                                    </div>
                                    <div class="mt-2">
                                        <p><?php
                                                $g = getGenre($film['idfilm'], true);
                                                $i = 0;
                                                if (count($g) > 1) {
                                                    foreach ($g as $val) {
                                                        $slash = (count($g) - $i > 1 ? " / " : "");
                                                        echo $val['genrefilm'] . $slash;
                                                        $i += 1;
                                                    }
                                                } else {
                                                    echo $g[0]['genrefilm'];
                                                }
                                                ?></p>
                                    </div>
                                    <div class="mt-2">
                                        <?php if (!empty($_SESSION['username'])) : ?>
                                        <?php $wishlist = get_where('user_favorite', ['iduser' => $_SESSION['iduser'], 'idfilm' => $film['idfilm']]); ?>
                                        <button type="button" onclick="sendData(this)"
                                            class="btn btn-block btn-<?= empty($wishlist) ? "primary" : "secondary" ?>"
                                            data-id="<?= $film['idfilm'] ?>" data-page="movie"
                                            data-action="<?= empty($wishlist) ? "add" : "remove" ?>"><?= empty($wishlist) ? "Add to wishlist" : "Remove from wishlist" ?></button>
                                        <?php else : ?>
                                        <a href="<?= base_url('login.php') ?>" class="btn btn-block btn-primary">Add to
                                            wishlist</a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="mid-2 agile_mid_2_home">
                                        <p><?= $film['tahunrilis'] ?></p>
                                        <div class="block-stars">
                                            <ul class="w3l-ratings">
                                                <?php for ($i = 0; $i < floor($film['rating'] / 2); $i++) : ?>
                                                <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                                <?php endfor; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- //movie-browse-agile -->

    </div>
    <!-- //w3l-medile-movies-grids -->
</div>

<!-- Load parsing header -->
<?php load('templates/footer.php') ?>
<!-- End load of header -->